#coding:utf-8

import unittest

from Sudoku import Sudoku

class TestSudoku(unittest.TestCase):
    def test_resolve(self):
        sudoku1 = Sudoku()
        sudoku2 = Sudoku()

        # 通常登録
        self.assertEqual(sudoku1.resolve('023000000070000030005000010201079340084100002037004100300000001010800065700000093'), True)
        self.assertEqual(sudoku1.print_field(False, '', True),'623941587179658234845327619261579348584136972937284156398765421412893765756412893')

        # 通常登録 異常系
        self.assertEqual(sudoku1.resolve(''),      False)
        self.assertEqual(sudoku1.resolve('12345'), False)

        
        # 短縮入力
        self.assertEqual(sudoku1.resolve('059033028201902480670280196704701105910153012084103106603401901'), True)
        self.assertEqual(sudoku1.print_field(False, '', True),'752649183316827954489531267238496715547218639691753428873962541924185376165374892')

        # 短縮入力 異常系
        self.assertEqual(sudoku1.resolve('1230770530350410120110179340284104201370241023071011018036570693'), False)


        # 再帰解析と候補解析が同値
        self.assertEqual(sudoku1.resolve('059033028201902480670280196704701105910153012084103106603401901'), True)
        self.assertEqual(sudoku2.resolve('059033028201902480670280196704701105910153012084103106603401901', None, False), True)
        self.assertEqual(sudoku1.print_field(False, '', True), sudoku2.print_field(False, '', True))


        # 配列入力
        filed = [[0, 2, 3, 0, 0, 0, 0, 0, 0],
                 [0, 7, 0, 0, 0, 0, 0, 3, 0],
                 [0, 0, 5, 0, 0, 0, 0, 1, 0],
                 [2, 0, 1, 0, 7, 9, 3, 4, 0],
                 [0, 8, 4, 1, 0, 0, 0, 0, 2],
                 [0, 3, 7, 0, 0, 4, 1, 0, 0],
                 [3, 0, 0, 0, 0, 0, 0, 0, 1],
                 [0, 1, 0, 8, 0, 0, 0, 6, 5],
                 [7, 0, 0, 0, 0, 0, 0, 9, 3]]

        self.assertEqual(sudoku1.resolve('', filed), True)
        self.assertEqual(sudoku1.print_field(False, '', True),'623941587179658234845327619261579348584136972937284156398765421412893765756412893')