#coding:utf-8

class Sudoku:
    """数独を解く！！
    """
    def __init__(self):
        self.field = [[0]*9 for i in range(9)]

    def __str__(self):
        return self._field_str(True, ' ')

    def _get_can_insert_value(self, y, x, field):
        """確認セルで確認しなくてはいけないセルに入っている値のリスト

        Args:
            y (int): 列座標
            x (int): 行座標
            field (list): 確認対象のフィールド

        Returns:
            list: 入力済みの値リスト
        """
        area_y, area_x = (y // 3) * 3, (x // 3) * 3
        area = sum([l[area_x:area_x+3] for l in field[area_y:area_y+3]], [])

        # return  [行 + 列 + エリア]
        return list(set(field[y] + [l[x] for l in field] + area))

    def _is_valid(self, y, x, field, value):
        """入れようとしている値が重複していないか確認する

        Args:
            y (int): 列座標
            x (int): 行座標
            field (list): 確認対象のフィールド
            value (int): 入るか確認する値

        Returns:
            bool: 確認結果 入る:True 入らない:False
        """
        return not value in self._get_can_insert_value(y, x, field)

    def _next_cell(self, field):
        """空白のセルの座標を返す

        Args:
            field (list): 確認対象のフィールド

        Returns:
            int: 未入力のy座標
            int: 未入力のx座標
        """
        for y in range(len(field)):
            for x in range(len(field[0])):
                if field[y][x] == 0:
                    return y, x
        return -1, -1

    def _input_field(self, field_str='', field=None):
        """フィールドの初期状態をメンバ変数に登録

        Args:
            field_str (str, optional): 区切りなし数字のみ1行文字列 デフォルトは ''
            field ([type], optional):  9x9の数値を要素に持つ2次元配列 デフォルトは None

        Returns:
            bool: True:入力成功, False:入力失敗,入力値が想定外
        """
        if field_str != '':
            field = list(field_str)
            if len(field) != 81:
                # セル数に足りていない場合は短縮入力を展開する
                zero_flg = False
                short_field = []
                for v in field:
                    if zero_flg:
                        short_field += ['0'] * (int(v) - 1)
                    else:
                        short_field.append(v)
                    zero_flg = (v == '0')
                field = short_field
            if len(field) != 81:
                return False
            self.field = [[int(n) for n in field[i:i+9]] for i in range(0, len(field), 9)]
        elif field != None:
            if len(field) != 9 or len(field[0]) != 9:
                return False
            self.field = field
        else:
            return False
        return True

    def _field_str(self, lines=True, separator=' '):
        """フィールドの内容を文字列で返す

        Args:
            lines (bool, optional): 改行の有無 True:改行あり, False:改行なし デフォルトは True
            separator (str, optional): 区切り文字 デフォルトは ' '

        Returns:
            str: フィールド状態の文字列
        """
        end = '\n' if lines else ''
        return end.join([separator.join(map(str, l)) for l in self.field])

    def print_field(self, lines=True, separator=' ', _return=False):
        """Field全体を表示する

        Args:
            lines (bool, optional): 改行の有無 True:改行あり, False:改行なし デフォルトは True
            separator (str, optional): 区切り文字 デフォルトは ' '
            _return (bool, optional): 標準出力の有無 出力しない場合は True デフォルトは False

        Returns:
            str|None: 出力しない場合は文字列, 出力した場合はNone
        """
        field = self._field_str(lines, separator)
        if _return:
            return field
        else:
            print(field)
            return None

    def _resolve_candidate(self):
        """1つのセルに入る候補が1つなら挿入する
        """
        while True:
            insert_flg = False
            
            # 全セルの候補を算出
            candidate_list = [[{0}]*9 for i in range(9)]
            for y in range(len(self.field)):
                for x in range(len(self.field[y])):
                    if self.field[y][x] == 0:
                        candidate_nums = {0,1,2,3,4,5,6,7,8,9} - set(self._get_can_insert_value(y, x, self.field))
                        if len(candidate_nums) == 1:
                            self.field[y][x] = list(candidate_nums)[0]
                        candidate_list[y][x] = candidate_nums
                    else:
                        candidate_list[y][x] = set([self.field[y][x]])

            # 候補が一つなら挿入する
            for y in range(len(self.field)):
                for x in range(len(self.field[y])):
                    if self.field[y][x] != 0:
                        continue

                    # 行内の他のセルには無い候補があればそれを挿入する
                    duplicate_flg = False
                    other_candidate = candidate_list[y][x]
                    for c in candidate_list[y]:
                        if candidate_list[y][x] == c:
                            if duplicate_flg:
                                other_candidate = other_candidate - c
                                break
                            duplicate_flg = True
                            continue
                        other_candidate = other_candidate - c
                    if len(other_candidate) == 1:
                        self.field[y][x] = list(other_candidate)[0]
                        insert_flg = True

                    # 行内の他のセルには無い候補があればそれを挿入する
                    duplicate_flg = False
                    other_candidate = candidate_list[y][x]
                    for c in [l[x] for l in candidate_list]:
                        if candidate_list[y][x] == c:
                            if duplicate_flg:
                                other_candidate = other_candidate - c
                                break
                            duplicate_flg = True
                            continue
                        other_candidate = other_candidate - c
                    if len(other_candidate) == 1:
                        self.field[y][x] = list(other_candidate)[0]
                        insert_flg = True

                    # 同グループ内の他のセルには無い候補があればそれを挿入する
                    duplicate_flg = False
                    other_candidate = candidate_list[y][x]
                    area_y, area_x = (y // 3) * 3, (x // 3) * 3
                    area = sum([l[area_x:area_x+3] for l in candidate_list[area_y:area_y+3]], [])
                    for c in area:
                        if candidate_list[y][x] == c:
                            if duplicate_flg:
                                other_candidate = other_candidate - c
                                break
                            duplicate_flg = True
                            continue
                        other_candidate = other_candidate - c
                    if len(other_candidate) == 1:
                        self.field[y][x] = list(other_candidate)[0]
                        insert_flg = True
            if not insert_flg:
                break

    def _resolve(self, field):
        """1つのセルに入る値を探索する 再帰呼び出しされる

        Args:
            field (list): 確認対象のフィールド

        Returns:
            bool: True:探索成功, False:探索失敗,入る値無し
        """
        y, x = self._next_cell(field)
        if y == -1 and x == -1:
            self.field = field
            return True

        for i in range(1, 10):
            if self._is_valid(y, x, field, i):
                field[y][x] = i
                if self._resolve(field):
                    return True
        field[y][x] = 0
        return False

    def resolve(self, field_str = '', field_list = None, condidate_way = True):
        """数独解析を開始するメンバ変数に探索するフィールドを登録する

        Args:
            field_str (str, optional): 区切りなし数字のみ1行文字列 デフォルトは ''
            field_list ([type], optional): 9x9の数値を要素に持つ2次元配列 デフォルトは None

        Returns:
            bool: True:探索成功, False:探索失敗
        """
        if not self._input_field(field_str, field_list):
            return False

        # 候補1通りなら挿入
        if condidate_way:
            self._resolve_candidate()

        # 再帰総当たり探索
        return self._resolve(self.field)

if __name__ == '__main__':
    sudoku = Sudoku()
    # sudoku.resolve('023000000070000030005000010201079340084100002037004100300000001010800065700000093')
    sudoku.resolve('01230770530350410120110179340284104201370241023071011018036570693')
    # sudoku.resolve('394725861671389524582416397837641952409870136216593748923167485168954273705208609')
    sudoku.print_field()